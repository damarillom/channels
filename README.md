# Channels

## Name

Media Channels

## Description

The project consists in a backend for a media platform, on it you can consult the Channels, their Subchannels and the Contents of all of them (it includes the info of them).
The project also have a command for calculate the rating of the channels and generate it on ordered CSV.

## Start project

docker compose build
docker compose up

## Execute average command

docker compose exec web bash
python manage.py get_channels_ratings

