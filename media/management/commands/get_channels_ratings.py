from django.core.management.base import BaseCommand
from django.db.models import Avg, Q
from django.db.models.functions import Coalesce
from typing import Any, Dict, List
import csv
from media.models import Channel

class Command(BaseCommand):
    help: str = "Calculate the average ratings for the channels and export it to CSV."

    def handle(self, *args: Any, **options: Dict[str, Any]) -> None:
        filename: str = "channels_ratings.csv"
        with open(filename, "w", newline="") as csvfile:
            fieldnames: List[str] = ["channel title", "average rating"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            channels = Channel.objects.annotate(
                average_rating=Coalesce(
                    Avg('subchannels__contents__rating', filter=Q(subchannels__isnull=False)),
                    Avg('contents__rating'),
                )
            ).order_by('-average_rating')
            self.stdout.write(self.style.SUCCESS(f'{channels}'))
            for ch in channels:
                writer.writerow(
                    {
                        "channel title": ch.name,
                        "average rating": ch.average_rating if ch.average_rating else 0.0, # type: ignore
                    }
                )

        self.stdout.write(self.style.SUCCESS(f'File "{filename}" generated correctly.'))