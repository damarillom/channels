from django.test import TestCase
from rest_framework.test import APIClient
from django.core.management import call_command
from media.models import Channel, Content, Group
import csv
import os


class TestChannelsRatingsCommand(TestCase):
    def setUp(self) -> None:
        self.channel1: Channel = Channel.objects.create(name="Channel1")
        self.channel2: Channel = Channel.objects.create(name="Channel2")
        self.subchannel1: Channel = Channel.objects.create(name="Subchannel1", parent_channel=self.channel1)
        self.subchannel2: Channel = Channel.objects.create(name="Subchannel2", parent_channel=self.channel1)
        Content.objects.create(title="Content1", rating=1, channel=self.channel2, metadata={})
        Content.objects.create(title="Content2", rating=3, channel=self.channel2, metadata={})
        Content.objects.create(title="Content3", rating=5, channel=self.subchannel1, metadata={})
        Content.objects.create(title="Content4", rating=2, channel=self.subchannel1, metadata={})
        Content.objects.create(title="Content5", rating=4, channel=self.subchannel2, metadata={})
        Content.objects.create(title="Content6", rating=8, channel=self.subchannel2, metadata={})

    def file_exists(self, filename: str) -> bool:
        return os.path.exists(filename)

    def test_command_output(self) -> None:
        call_command("get_channels_ratings")
        self.assertTrue(self.file_exists("channels_ratings.csv"))

        with open("channels_ratings.csv", "r") as csvfile:
            reader = csv.DictReader(csvfile)
            rows: list = list(reader)
            self.assertEqual(len(rows), 4)
            self.assertEqual(rows[0]["channel title"], "Subchannel2")
            self.assertEqual(rows[0]["average rating"], '6.0')
            self.assertEqual(rows[1]["channel title"], "Channel1")
            self.assertEqual(rows[1]["average rating"], '4.75')
            self.assertEqual(rows[2]["channel title"], "Subchannel1")
            self.assertEqual(rows[2]["average rating"], '3.5')
            self.assertEqual(rows[3]["channel title"], "Channel2")
            self.assertEqual(rows[3]["average rating"], '2.0') 


class ChannelModelTest(TestCase):
    def test_channel_creation(self) -> None:
        channel: Channel = Channel.objects.create(name="Channel")
        self.assertTrue(isinstance(channel, Channel))
        self.assertEqual(channel.__str__(), channel.name)


class ContentModelTest(TestCase):
    def test_content_creation(self) -> None:
        channel: Channel = Channel.objects.create(name="Channel")
        content: Content = Content.objects.create(title="Content", rating=5.0, metadata={}, channel=channel)
        self.assertTrue(isinstance(content, Content))
        self.assertEqual(content.__str__(), content.title)


class GroupModelTest(TestCase):
    def test_group_creation(self) -> None:
        group: Group = Group.objects.create(name="Group")
        self.assertTrue(isinstance(group, Group))
        self.assertEqual(group.__str__(), group.name)


class EndpointTest(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.channel1: Channel = Channel.objects.create(name="Channel1")
        self.channel2: Channel = Channel.objects.create(name="Channel2")
        self.subchannel1: Channel = Channel.objects.create(name="Subchannel1", parent_channel=self.channel1)
        self.subchannel2: Channel = Channel.objects.create(name="Subchannel2", parent_channel=self.channel1)
        Content.objects.create(title="Content1", rating=1, channel=self.channel2, metadata={})
        Content.objects.create(title="Content2", rating=3, channel=self.channel2, metadata={})
        Content.objects.create(title="Content3", rating=5, channel=self.subchannel1, metadata={})
        Content.objects.create(title="Content4", rating=2, channel=self.subchannel2, metadata={})
        Content.objects.create(title="Content5", rating=8, channel=self.subchannel2, metadata={})
        self.group1: Group = Group.objects.create(name="Group1")
        self.group1.channels.add(self.channel2.pk)
        self.group2: Group = Group.objects.create(name="Group2")
        self.group2.channels.add(self.subchannel1.pk, self.subchannel2.pk)
        self.group3: Group = Group.objects.create(name="Group3")
        self.group3.channels.add(self.subchannel1.pk)
        self.group4: Group = Group.objects.create(name="Group4")

    def test_channel_endpoint(self) -> None:
        response = self.client.get('/api/channels/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], 'Channel2')
        self.assertEqual(len(response.data[0]['contents']), 2)
        self.assertEqual(len(response.data[0]['subchannels']), 0)
        self.assertEqual(response.data[1]['name'], 'Channel1') 
        self.assertEqual(len(response.data[1]['contents']), 0)
        self.assertEqual(len(response.data[1]['subchannels']), 2)
        self.assertEqual(len(response.data[1]['subchannels'][0]['contents']), 1)
        self.assertEqual(len(response.data[1]['subchannels'][1]['contents']), 2)

    def test_channel_with_filter_endpoint(self) -> None:
        # type: ignore
        response1 = self.client.get('/api/channels/?group=Group1')
        self.assertEqual(len(response1.data), 1)
        self.assertEqual(response1.data[0]['name'], 'Channel2')
        response2 = self.client.get('/api/channels/?group=Group2')
        self.assertEqual(len(response2.data), 1)
        self.assertEqual(response2.data[0]['name'], 'Channel1')
        response3 = self.client.get('/api/channels/?group=Group3')
        self.assertEqual(len(response3.data), 1)
        self.assertEqual(response3.data[0]['name'], 'Channel1')
        response4 = self.client.get('/api/channels/?group=Group4')
        self.assertEqual(len(response4.data), 0)

    def test_subchannel_endpoint(self) -> None:
        response = self.client.get('/api/subchannels/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], 'Subchannel2')
        self.assertEqual(response.data[0]['parent_channel']['name'], 'Channel1')
        self.assertEqual(len(response.data[0]['contents']), 2)
        self.assertEqual(response.data[1]['name'], 'Subchannel1')
        self.assertEqual(response.data[1]['parent_channel']['name'], 'Channel1')
        self.assertEqual(len(response.data[1]['contents']), 1)

    def test_subchannel_with_filter_endpoint(self) -> None:
        response1 = self.client.get('/api/subchannels/?group=Group1')
        self.assertEqual(len(response1.data), 0)
        response2 = self.client.get('/api/subchannels/?group=Group2')
        self.assertEqual(len(response2.data), 2)
        self.assertEqual(response2.data[0]['name'], 'Subchannel2')
        self.assertEqual(response2.data[1]['name'], 'Subchannel1')
        response3 = self.client.get('/api/subchannels/?group=Group3')
        self.assertEqual(len(response3.data), 1)
        self.assertEqual(response3.data[0]['name'], 'Subchannel1')

    def test_content_endpoint(self) -> None:
        response = self.client.get('/api/contents/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 5)
