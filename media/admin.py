from django.contrib import admin
from .models import Channel, Content, Group


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    raw_id_fields = ('parent_channel',)


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'rating')
    raw_id_fields = ('channel',)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    raw_id_fields = ('channels',)
