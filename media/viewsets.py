from rest_framework import viewsets
from .models import Channel, Content
from .serializers import ChannelSerializer, SubchannelSerializer, ContentSerializer
from django.db.models import QuerySet, Q
from typing import Optional


class ChannelViewSetBase(viewsets.ModelViewSet):
    filter_fields = ('groups__name',)

    def get_queryset(self) -> QuerySet:
        qs: QuerySet = super().get_queryset()
        group_name: Optional[str] = self.request.query_params.get('group')
        if group_name:
            qs = qs.filter(Q(groups__name=group_name) | Q(subchannels__groups__name=group_name)).distinct()
        return qs


class ChannelViewSet(ChannelViewSetBase):
    queryset = Channel.objects.filter(parent_channel__isnull=True).order_by('-id')
    serializer_class = ChannelSerializer


class SubchannelViewSet(ChannelViewSetBase):
    queryset = Channel.objects.filter(parent_channel__isnull=False).order_by('-id')
    serializer_class = SubchannelSerializer


class ContentViewSet(viewsets.ModelViewSet):
    queryset = Content.objects.all().order_by('-id')
    serializer_class = ContentSerializer
