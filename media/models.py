from django.db import models
from typing import Optional


class Channel(models.Model):
    name: models.CharField = models.CharField(max_length=255)
    language: models.CharField = models.CharField(max_length=255)
    picture: models.ImageField = models.ImageField(upload_to="channel_pictures/")
    parent_channel: Optional[models.ForeignKey] = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name="subchannels")

    def __str__(self) -> str:
        name: str = self.name
        return name


class Content(models.Model):
    title: models.CharField = models.CharField(max_length=255)
    file: models.FileField = models.FileField(upload_to="contents/")
    metadata: models.JSONField = models.JSONField()
    rating: models.FloatField = models.FloatField()
    channel: models.ForeignKey = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name="contents")

    def __str__(self) -> str:
        title: str = self.title
        return title
    

class Group(models.Model):
    name: models.CharField = models.CharField(max_length=255)
    channels: models.ManyToManyField = models.ManyToManyField(Channel, related_name="groups")

    def __str__(self) -> str:
        name: str = self.name
        return name