from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .viewsets import ChannelViewSet, SubchannelViewSet, ContentViewSet

router = DefaultRouter()
router.register(r"channels", ChannelViewSet)
router.register(r"subchannels", SubchannelViewSet)
router.register(r"contents", ContentViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
