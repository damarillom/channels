from rest_framework import serializers
from .models import Channel, Content


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = "__all__"


class ChannelBaseSerializer(serializers.ModelSerializer):
    contents = ContentSerializer(many=True, read_only=True)

    class Meta:
        model = Channel
        fields = ("id", "name", "language", "picture", "contents")
        abstract = True


class SubchannelSerializer(ChannelBaseSerializer):
    class Meta:
        model = Channel
        fields = ChannelBaseSerializer.Meta.fields + ("parent_channel",)
        depth = 1


class ChannelSerializer(ChannelBaseSerializer):
    subchannels = SubchannelSerializer(many=True, read_only=True)

    class Meta:
        model = Channel
        fields = ChannelBaseSerializer.Meta.fields + ("subchannels",)
        depth = 1
