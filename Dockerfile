FROM python:3.8

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=channels.settings

COPY . /app/
COPY requirements.txt /app/

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

# # Dar permisos de ejecución al script de entrada
RUN chmod +x /app/entrypoint.sh

# # Punto de entrada del contenedor
ENTRYPOINT ["/app/entrypoint.sh"]